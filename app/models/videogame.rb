class Videogame < ApplicationRecord
    
    extend FriendlyId
    friendly_id :name, use: :slugged
    
    belongs_to :admin

    
    scope :most_recent, -> {order(id: :desc) } 
    
    def should_generate_new_friendly_id?
        slug.blank? || name_changed?
    end
    
    def display_date_published
        "#{created_at.strftime('%-b %-d, %Y')}";
    end
        
end
