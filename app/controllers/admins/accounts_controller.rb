module Admins
    class AccountsController < AdminsController
        def edit
        end
        def update_info
          if current_admin.update(admin_info_params)
            flash[:success] = 'Successfully saved info.'
          else
            flash[:danger] = current_admin.display_error_messages
          end
          redirect_to admins_account_path
        end

        def update_password

        end

        private

        def admin_info_params
          params.require(:admin).permit(:name, :email, :bio)
        end

        def admin_password_params
          params.require(:admin).permit(:current_password, :new_password, :new_password_confirmation)
        end

      end
end