module User
    class VideogamesController < UserController

    # GET /videogames
    # GET /videogames.json
        def index
            @videogames = Videogame.most_recent
        end

        # GET /videogames/1
        # GET /videogames/1.json
        def show
            @videogame = Videogame.friendly.find(params[:id])
        end
    end
end