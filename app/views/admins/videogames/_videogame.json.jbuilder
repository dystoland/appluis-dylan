json.extract! videogame, :id, :name, :description, :slug, :created_at, :updated_at
json.url videogame_url(videogame, format: :json)
