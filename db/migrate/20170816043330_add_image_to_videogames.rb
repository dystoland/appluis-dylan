class AddImageToVideogames < ActiveRecord::Migration[5.0]
  def change
    add_column :videogames, :image, :string
  end
end
