class CreateVideogames < ActiveRecord::Migration[5.0]
  def change
    create_table :videogames do |t|
      t.string :name
      t.text :description
      t.string :slug

      t.timestamps
    end
    add_index :videogames, :slug, unique: true
  end
end
