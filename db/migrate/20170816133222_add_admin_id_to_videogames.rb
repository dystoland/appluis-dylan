class AddAdminIdToVideogames < ActiveRecord::Migration[5.0]
  def change
    add_column :videogames, :admin_id, :integer
    add_index :videogames, :admin_id
  end
end
