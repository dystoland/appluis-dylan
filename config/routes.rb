Rails.application.routes.draw do
  
  devise_for :admins
  root to: 'user/videogames#index'
    

  namespace :admins do
      get '/account' => 'accounts#edit', as: :account
      put '/info' => 'accounts#update_info', as: :info
      put '/password' => 'accounts#update_password', as: :password
      resources :videogames
  end
    
    scope module: 'user' do
        get 'about' => 'pages#about', as: :about
        get 'contact' => 'pages#contact', as: :contact
        get 'videogames' => 'videogames#index', as: :videogames
        get 'videogames/:id' => 'videogames#show', as: :videogame
    end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
